mod state;

use voip::packet::Packet;

use std::net::{SocketAddr, UdpSocket};
use std::sync::{Mutex, Arc};
use std::thread;

use state::VoipServer;

fn main() -> std::thread::Result<()> {

    //Is there a better way to do this?
    let rec     = VoipServer::new();
    let decode  = rec.clone();
    let process = decode.clone();
    let rec_thread = thread::spawn(|| -> () {
        rec.rec_loop();
    });

    let decode_thread = thread::spawn(|| -> () {
        decode.decode_loop();
    });

    let process_thread = thread::spawn(|| -> () {
        process.proc_loop();
    });

    rec_thread.join()?;
    decode_thread.join()?;
    process_thread.join()?;

    Ok(())
}
