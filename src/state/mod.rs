use std::net::{SocketAddr, UdpSocket};
use std::sync::{Mutex, Arc};

use voip::room::RoomId;
use voip::packet::Packet;
use voip::packet::UserId;
use std::collections::HashMap;

type Arm<T> = Arc<Mutex<T>>;

mod sockets {
    use std::net::{UdpSocket, SocketAddr};

    lazy_static::lazy_static! {
        pub static ref SOCK: UdpSocket = {
            let addr: SocketAddr = "127.0.0.1:3333".parse().unwrap();
            UdpSocket::bind(&addr).expect("failed to bind to socket")
        };
    }
}

#[derive(Debug)]
pub struct ReceivedPacket(std::net::SocketAddr, Packet);

#[derive(Clone, Default)]
pub struct VoipServer {
    pub msgs:   Arm<Vec<ReceivedPacket>>,
    pub buffer: Arm<Vec<u8>>,
    pub queued: Arm<Vec<(usize, SocketAddr)>>,

    pub conn_map: Arm<HashMap<SocketAddr, Arc<voip::user::User>>>,
    pub user_map: Arm<HashMap<UserId,     Arc<voip::user::User>>>,

    pub room_map: Arm<HashMap<RoomId, voip::room::Room>>,
}

impl VoipServer {
    pub fn new() -> Self {
        VoipServer::default()
    }

    pub fn decode_loop(self) {
        use std::time::Duration;
        const SLEEP_TIME: Duration = Duration::from_millis(200);
        
        loop {
            let now = std::time::Instant::now();

            let mut buffer = Vec::new();
            let mut queued = Vec::new();

            {
                let mut buffer_ref = self.buffer
                    .lock()
                    .expect("failed to lock 'buffer' mutex");

                let mut queued_ref = self.queued
                    .lock()
                    .expect("failed to lock 'queued' mutex");


                std::mem::swap(&mut buffer, &mut buffer_ref);
                std::mem::swap(&mut queued, &mut queued_ref);
            }

            let mut msg_buff = self.msgs
                .lock()
                .expect("unable to lock message buffer");

            for (siz, addr) in queued.into_iter() {
                let dat: Vec<u8> = buffer.drain(0..siz)
                    .collect();

                match bincode::deserialize::<voip::packet::Packet>(&dat) {
                    Ok(packet) => msg_buff.push(ReceivedPacket(addr, packet)),
                    Err(err)   => eprintln!("failed to deserailize packet '{}'", err)
                }

                println!("received packet");
            }

            drop(msg_buff);

            std::thread::sleep(SLEEP_TIME - now.elapsed());
        }
    }

    pub fn rec_loop(self) {
        let mut temp_buffer: Vec<u8> = vec![0; 2048];
        loop {
            let (siz, addr) = sockets::SOCK.recv_from(&mut temp_buffer)
                .expect("failed to receive message");

            let mut buffer = self.buffer
                .lock()
                .expect("failed to lock 'buffer' list");
            let mut queued = self.queued
                .lock()
                .expect("failed to lock 'queued' list");

            buffer.extend(temp_buffer[0..siz].iter());
            queued.push((siz, addr));
        }
    }

    fn send_packet(packet: Packet, dest: SocketAddr) {
        let buff: Vec<u8> = bincode::serialize(&packet).unwrap();
        sockets::SOCK.send_to(&buff, &dest)
            .expect(&format!("failed to send message to loc '{}", dest));
    }

    pub fn proc_loop(self) {
        loop {
            let mut fill = Vec::new();
            {
                let mut msgs = self.msgs
                    .lock()
                    .expect("failed to lock 'msgs' buffer");

                std::mem::swap(&mut fill, &mut msgs);
            }

            for ReceivedPacket(addr, packet) in fill.into_iter() {
                match packet {
                    Packet::RequestJoin(room) => {
                        let yes = Packet::SuccessfulJoin(room);
                        VoipServer::send_packet(yes, addr);
                    }
                    _ => unimplemented!()
                }
            }
        }
    }
}
